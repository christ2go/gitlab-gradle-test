public class Ship {
    Position pos;
    int hp;
    public Ship(Position position, int health) {
        hp = health;
        pos = position;
    }

    void decreaseHealthPoints(int x) {
        hp = Integer.max(0, hp-x);
    }

    public boolean isAlive() {
        return hp > 0;
    }

    public int getHealth() {
        return hp;
    }

    public boolean move(Position p) {
        if(Position.distance(p, pos) <= 1) {
            p = pos;
            return true;
        }
        return false;
    }
}
