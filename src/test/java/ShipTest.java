import org.junit.Assert;
import org.junit.Test;

public class ShipTest {
    @Test
    public void aliveTest() {
        Ship s = new Ship(new Position(2, 4), 3);
        s.decreaseHealthPoints(3);
        Assert.assertFalse(s.isAlive());
    }

    @Test
    public void decreaseHpTest() {
        Ship s = new Ship(new Position(2, 4), 3);
        s.decreaseHealthPoints(2);
        Assert.assertTrue(s.isAlive());
        Assert.assertEquals(1, s.getHealth());
    }

    @Test
    public void moveNextTest() {
        Ship s = new Ship(new Position(2, 4), 3);
        Assert.assertTrue(s.move(new Position(2, 3)));
        Assert.assertFalse(s.move(new Position(2,1)));
    }
}
